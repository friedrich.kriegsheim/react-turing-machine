export class TuringTape {
    constructor(){
        this.tape = [[],[]]
    }

    getIndexPair(index){
        const tapeIndex = index>=0?1:0;
        const cellIndex = index>=0?Math.abs(index):Math.abs(index+1);
        return [tapeIndex, cellIndex];

    }
    getValue(index){
        const [tapeIndex, cellIndex] = this.getIndexPair(index)
        return this.tape[tapeIndex][cellIndex]
    }
    setValue(index, value){
        const [tapeIndex, cellIndex] = this.getIndexPair(index)
        this.tape[tapeIndex][cellIndex] = value
    }

}

export default class TuringMachine{
    tape = new TuringTape();
    finalState = "qEnd"
    initialState = "q0"
    currentState = "q0"
    transitionFunction = { "q0": {}}
    currentPosition = 0
    configHistory = []



    step(){
        if (!this.currentState)
            this.currentState = this.initialState
        let currentCell = (this.tape && this.tape.getValue(this.currentPosition)) || ""
        if (this.currentState === this.finalState)  return 1
        if (!this.transitionFunction[this.currentState]
            || !this.transitionFunction[this.currentState][currentCell]
            ) 
            
            return -1

        if (this.configHistory.length === 0){
            this.configHistory[this.configHistory.length] = this.toString()
        }

        const transition = this.transitionFunction[this.currentState][currentCell];
        this.currentState = transition[0]
        this.tape.setValue(this.currentPosition, transition[1]==="B"?"":transition[1])
        if (transition[2] === "R") this.currentPosition++;
        if (transition[2] === "L") this.currentPosition--;
        this.configHistory[this.configHistory.length] = this.toString()
        return 0;
    }

    getCurrentInput(){
        return ((this.tape && this.tape.getValue(this.currentPosition)) || "")
    }

    toString(){

        const [tapeIndex, cellIndex] = this.tape.getIndexPair(this.currentPosition);
   
        const toStringMapLeft = (v,i)=>{
            return ` ${tapeIndex===0&&cellIndex===i?`${this.currentState} `:``}${v}`
        }
        const toStringMapRight = (v,i)=>{
            return ` ${tapeIndex===1&&cellIndex===i?`${this.currentState} `:``}${v}`
        }
        const left = this.tape.tape[0].map(toStringMapLeft).join('')
        const right = this.tape.tape[1].map(toStringMapRight).join('')
        const suffix = tapeIndex===1&&cellIndex>=this.tape.tape[1].length?` ${this.currentState}`:""
        return left + right + suffix;
    }

}

