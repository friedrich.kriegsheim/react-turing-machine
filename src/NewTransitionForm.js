import React, {useState, useRef, useEffect} from "react";
import './NewTransitionForm.css'
import ffwImg from './img/ffwd.png'
export function NewTransitionForm(props){
    const tm = props.tm
    const [transitionString, setTransitionString] = useState("");
    const [errMsg, setErrMsg] = useState("");
    const ref = useRef()
    useEffect(()=>{
        ref.current && ref.current.focus()
    })

    function handleAddTransiton(){
        const currentInput = tm.getCurrentInput()==="B"?"":tm.getCurrentInput()
        try {
            const newTransitionFunction = transitionString.split(',');

            if (newTransitionFunction.length !== 3) 
                throw new Error("Invalid argument count.")

            if (!newTransitionFunction[0] || newTransitionFunction[0] === "") 
                throw new Error("statename can't be empty")

            if (newTransitionFunction[0].indexOf(' ')>=0) 
                throw new Error("Invalid state name (can't contain spaces)")

            if (newTransitionFunction[1].length !== 1)
                throw new Error("write symbol needs to be length 1")

            if (!(newTransitionFunction[2] === "L" || newTransitionFunction[2] === "R" || newTransitionFunction[2] === "N")) 
                throw new Error("invalid move direction")

            if (!tm.transitionFunction[tm.currentState]) 
                tm.transitionFunction[tm.currentState] = {}

            if (!tm.transitionFunction[tm.currentState][currentInput]) 
                tm.transitionFunction[tm.currentState][tm.getCurrentInput()] = {}

            tm.transitionFunction[tm.currentState][currentInput] = transitionString.split(',')
            props.setNewTransition(false);
            props.handleConfirm();

        }catch (e){
            setErrMsg(e.toString())
        }

    }
    function handleKeyDown(e){
        if (e.key === "Enter"){
            handleAddTransiton()
        }

    }
    return( 
    <div>
        <h2>Create New Transition</h2>
        <div>
            The TM encountered a Configuration where it doesn't know what to do. Enter new transition function: What should  the TM do when it is in state <b>{tm.currentState}</b> and reads <b>{tm.getCurrentInput()===""?"B":tm.getCurrentInput()}</b>
            <div className="newTransitionWrapper">
                <span>{`(${tm.currentState},${tm.getCurrentInput()||"B"})\u2192`}
                </span>
                (
                <input 
                    ref={ref} 
                    className="newTransitionInput" 
                    onKeyDown={handleKeyDown} 
                    onChange={
                        (e)=>setTransitionString(e.target.value)
                    } 
                    type="text" 
                    placeholder={`q${Object.keys(tm.transitionFunction).length}, writeSymbol, {L,N,R}`}
                >
                </input>
                )
                <button 
                    className="tmControl tmControlPlay newTransitionPlay" 
                    onClick={handleAddTransiton}
                >
                    <img className="mediaButton" alt="fast forward icon" src={ffwImg}>
                    </img>
                </button>
                {' '} 
                <a
                    href="/#" 
                    style={{color:"white"}} 
                    onClick={
                        ()=>props.setNewTransition(false)
                    }
                >
                cancel
                </a>
                <div>
                    <span className="err">
                        {errMsg}
                    </span>
                </div>
            </div>
        </div>
       
    </div>
    )
}