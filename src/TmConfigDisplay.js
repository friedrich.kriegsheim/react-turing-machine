import React, {useState, useEffect, useRef} from "react"
import './TmConfigDisplay.css'
import leftArrowImg from './img/leftArrow.png'
import rightArrowImg from './img/rightArrow.png'
import moveLeftArrow from './img/moveLeftArrow.png'
import moveRightArrow from './img/moveRightArrow.png'

export function TmConfigDisplay(props){ 
  const [editing, setEditing] = useState(0)
  const [displayStartIndex, setDisplayStartIndex] = useState(-2)
  const [lastDisplayStartIndex, setLastDisplayStartIndex] = useState(-2)
  const [lastActive, setLastActive] = useState(0)

  const displayElemCount = props.displayElemCount
  const flagUpdate = props.flagUpdate
  const tm = props.tm
  const items = [];

  useEffect(()=>{
    setTimeout(()=>{
      if(lastDisplayStartIndex !== displayStartIndex){
        setLastDisplayStartIndex(displayStartIndex)
      }
    },333)
  })
  useEffect(()=>{
    setTimeout(()=>{
      if (lastActive !== tm.currentPosition)
      setEditing((tm.currentPosition-displayStartIndex)%displayElemCount)
      setLastActive(tm.currentPosition)

    },333)
  })
  function increaseDisplayRange(e){
    if (editing < displayElemCount - 1){
      setEditing(editing+1)
    }else{
      setDisplayStartIndex(displayStartIndex+1)
    }
  }

  function decreaseDisplayRange(){
    if(editing>0){
      setEditing(editing-1);
    }else{
      setDisplayStartIndex(displayStartIndex-1)
    }
  }

  /*------------------------------------------------------------------
                  <MoveTmHeaderButton>
  --------------------------------------------------------------------*/

  

  /*------------------------------------------------------------------
                  <InputCell/>
  ------------------------------------------------------------------*/

  function InputCell(inputCellProps){
    const textInput = useRef(null);
    useEffect(()=>{
      if (editing === inputCellProps.displayIndex ){
        textInput.current.focus();
      }
    })

    function handleCellValueChange(event){
      const newValue = event.target.value.charAt(event.target.value.length-1)
      if(newValue !== "")
        setEditing(editing+1%displayElemCount)
        increaseDisplayRange()
      tm.tape.setValue(inputCellProps.index, newValue)
      flagUpdate()

    }


    function handleFocus(){
        setEditing(inputCellProps.displayIndex);
    }



    function handleKeyDown(e){

      switch (e.key) {
        case "Enter":
          props.handleConfirm();
          break;
        case "ArrowRight":
          increaseDisplayRange(e);
          break;
        case "ArrowLeft":
          decreaseDisplayRange(e);
          break; 
        case "Backspace":
            tm.tape.setValue(inputCellProps.index, "")
            decreaseDisplayRange();
          break;
        default:
          break;
      }
    
    }

    return <input type="text"
      value = {tm.tape.getValue(inputCellProps.index)}
      ref={textInput}
      onChange={handleCellValueChange}
      onKeyDown={handleKeyDown}
      onFocus={handleFocus}
      className={(tm.currentPosition===inputCellProps.index&&"activeInputCell") || "inputCell"}
    ></input>
  }


  if (tm.currentPosition>displayStartIndex+displayElemCount-1 && lastActive !== tm.currentPosition){
    
    setDisplayStartIndex(tm.currentPosition - displayElemCount +1)
  }else if (tm.currentPosition<displayStartIndex && lastActive !== tm.currentPosition){
      setDisplayStartIndex(tm.currentPosition)
  }

  items.push(
    <div key={-1} className="displayCellWrap">
      <img className={lastDisplayStartIndex>displayStartIndex?"moveArrow moveArrowActive":"moveArrow"  }
        src={moveLeftArrow} 
        onClick={()=>setDisplayStartIndex(displayStartIndex-1)}
        alt="move tape display left" 
      >

        </img>
    </div>)



  for (let i=0; i<displayElemCount;i++){
    const CellIndexLabel = ()=>{ 
      if (displayStartIndex+i===-1)
        return <div className="cellIndexDisplay">{(displayStartIndex+i)}</div>
      if (!((displayStartIndex+i)%5))
        return <div className="cellIndexDisplay">{(displayStartIndex+i)}</div>
      return null
    }

    const MoveTmHeaderButton = 
      displayStartIndex+i !== tm.currentPosition?
        ()=>null:
        (props)=>{
          function moveHeader(move){
            tm.currentPosition += move; 
            flagUpdate()
          }
  
          let img = null;
          let alt = "";
      
          if (props.direction > 0){
            img = rightArrowImg;
            alt = "Move TM Header one cell to the right"
          }
          if (props.direction <0){
            img = leftArrowImg;
            alt = "Move TM header one cell to the left"
          }
      
          if (props.active){
            return( 
              <a 
                href="/#" 
                onClick={()=>moveHeader(props.direction)}
              >
                <img className={"moveTmHeaderButton " +(props.highlight?"activeMoveTmHeaderButton":"")}
                    src={img}
                    alt={alt}>
                </img>
              </a>  
            
            )
          }else{
            return null;
          }
        }
      
    

    items.push(      
          <div key={i} className="displayCellWrap">
            <div className="displayState">
              <MoveTmHeaderButton
                active={displayStartIndex+i === tm.currentPosition}
                direction={-1}
                highlight={tm.currentPosition<lastActive}
              />
              { displayStartIndex+i===tm.currentPosition&&((tm&&tm.currentState)) }
              <MoveTmHeaderButton
                active={displayStartIndex+i === tm.currentPosition}
                direction={1}
                highlight={tm.currentPosition>lastActive}
              />
              </div>
            <InputCell 
              displayIndex = {i}
              index={displayStartIndex+i}
            />
            <CellIndexLabel/>
          </div>
    )
  }

  items.push(
    <div key={displayElemCount} className="displayCellWrap">
      <img className={lastDisplayStartIndex<displayStartIndex?"moveArrow moveArrowActive":"moveArrow" }
        alt="move tape display right" 
        src={moveRightArrow} 
        onClick={()=>setDisplayStartIndex(displayStartIndex+1)}
      >

        </img>
    </div>)


  return items
}