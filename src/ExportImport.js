import React, { useState, useRef, useEffect } from 'react'
import './ExportImport.css'
import { TuringTape } from './TuringMachine'



const ResetDialog = function(props){
    const handleResetEverything = props.handleResetEverything;
    const handleResetStateAndInput = props.handleResetStateAndInput;

    return <div className="exportTMDialog">
        <h2>Reset Turing mache</h2>
        Do you want to reset everything or just the input and current state of the Turing machine?
        <div style={{marginTop: "20px"}}>
            <div style={{float: "left", textAlign:"left"}}>
                <a href="/#" className="button">cancel</a>
            </div>
        <div style={{float: "right",textAlign: "right"}} >
            <a href="/#" className="button err" onClick={()=>{ handleResetEverything(); props.setExportState(null)}}>Reset everything</a>
            {' '}
            <a href="/#" className="button warn" onClick={()=> { handleResetStateAndInput(); props.setExportState(null)}}>Reset state & input</a>

        </div>
        </div>
    </div>
}

const ExportDialog = function(props){
    const [exportString, setExportString] = useState(JSON.stringify(props.tm,null,'\t'))
    const ref = useRef(null);
    const [textAreaHight, setTextAreaHight] = useState("")

    useEffect (()=>{
        if (ref.current){
            setTextAreaHight(ref.current.scrollHeight+"px");
        }
    },[exportString])
    return( 
       <div
        className="exportTMDialog">
            <h2> Export Turing machine</h2>
            <div className="exportOptionsWrapper">
                <a href="/#" className="button" onClick={handleSetExportString(false)}>Export TM</a>
                {' '}
                <a href="/#" className="button" onClick={handleSetExportString(true)}>Export TM & current state</a>
                <div>
                <textarea className="exportStringTextArea"  ref={ref} style={{height: textAreaHight}}readOnly value={exportString}> </textarea>
                </div>
                <div style={{textAlign: "right"}}>
                    <a href="/#" className={"button success"} onClick={()=>{props.setExportState(null)}}>Done</a>
                </div>
            </div>
        </div>
    )


    function handleSetExportString(exportState){
        if (!exportState) return ()=> {

            const tmCopy = Object.assign({}, props.tm)
            tmCopy.tape.tape[0] = []
            tmCopy.tape.tape[1] = []
            tmCopy.currentState = props.tm.initialState;
            tmCopy.currentPosition = 0
            tmCopy.configHistory = []
            setExportString(JSON.stringify(tmCopy,null,'\t'))
        }
        return ()=> setExportString(JSON.stringify(props.tm,null,'\t'));
    }
}

const ImportDialog = function(props){
    const [importString, setImportString] = useState("")
    const [errMsg, setErrMsg] = useState("")
    const ref = useRef(null);
    const [textAreaHight, setTextAreaHight] = useState("")

    useEffect (()=>{
        if (ref.current){
            setTextAreaHight(ref.current.scrollHeight+"px");
        }
    },[importString])
    return( 
       <div
        className="exportTMDialog">
            <h2> Import Turing machine</h2>
            <div className="exportOptionsWrapper">

                <div>
                <textarea className="exportStringTextArea"  ref={ref} style={{height: textAreaHight}} onChange={(e)=>{setImportString(e.target.value);}}value={importString}> </textarea>
                </div>
                <div className="errBox err">
                {errMsg}
                </div>
                <div style={{textAlign: "right"}}>
                    <a className="button err" href="/#" onClick={()=>{props.setExportState(null)}}>Abort</a>
                    {' '}
                    <a className="button success" href="/#" onClick={importFromString}>Import</a>
                </div>
            </div>
        </div>
    )


    function importFromString(exportState){
        setErrMsg("")
        try{ 
            const tmObj=JSON.parse(importString)
            Object.assign(props.tm, tmObj)
            props.tm.tape = new TuringTape()
            props.tm.tape.tape[0] = [...tmObj.tape.tape[0]]
            props.tm.tape.tape[1] = [...tmObj.tape.tape[1]]
            props.setExportState(null)
            props.flagUpdate()
        }catch(e){
            setErrMsg(errMsg + e.toString())
        }
    }
}

export function ExportImportGroup(props){
    const[exportState,setExportState] = useState("");
    let dialog = null
    switch (exportState){
        case "export": 
            dialog = <ExportDialog tm={props.tm} setExportState={setExportState}></ExportDialog>
            break;
        case "import": 
            dialog = <ImportDialog setExportState={setExportState} tm={props.tm} setTm={props.tm} flagUpdate={props.flagUpdate}></ImportDialog>
            break;
        case "reset":
            dialog = <ResetDialog 
                        resetState={props.resetState} 
                        setExportState={setExportState} 
                        handleResetEverything = {props.handleResetEverything}
                        handleResetStateAndInput = {props.handleResetStateAndInput}
                        flagUpdate={props.flagUpdate}>
                    </ResetDialog>
            break;
        default:
            dialog = null;
            break;
    }
    return(
        <div className="exportImportButtonsWrapper">
            <a className="button success" href="/#"onClick={()=>setExportState("export")}>Export</a>{dialog}
            {' '}
            <a className="button warn" href="/#" onClick={()=>setExportState("import")}>Import</a>
            {' '}
            <a className="button err" href="/#" onClick={()=>setExportState("reset")}>Reset</a>
        </div>)






}