import React, {useEffect, useState} from 'react';
import './App.css';
import TuringMachine from './TuringMachine';
import {useWindowWidth} from './useCustomEffects'
import { TmConfigDisplay } from './TmConfigDisplay'
import { NewTransitionForm} from './NewTransitionForm';
import TmCurrentConfig from './TmCurrentConfig';
import { ExportImportGroup } from './ExportImport';
import ffwImg from './img/ffwd.png'
import playImg from './img/play.png'
import pauseImg from './img/pause.png'


function App() {

  //states
  const [tmRunning, setTmRunning] = useState(false);
  const [tm,setTm] = useState(new TuringMachine())
  const [update, setUpdate] = useState(false);
  const [newTransition, setNewTransition] = useState(false);
  const  windowWidth = useWindowWidth()


  useEffect(()=>{
    const timer = setTimeout(fastForward,1000);
    return () => clearTimeout(timer)
  })
  
  //calculate how many items tape cells can be displayed
  const calcelemCount = Math.floor((windowWidth)/56)-3
  const  displayElemCount = calcelemCount > 0? calcelemCount : 1;

  /*-----------------------------------------------------------------------------------------
        PASSDOWN FUNCTIONS
  -------------------------------------------------------------------------------------------*/


  //function to pass to child components that can be called to trigger re-render after tm has been altered
  function flagUpdate(){
    setUpdate(!update)
  }


  function handleNextStep(){
    const stepVal = tm.step()
    if (stepVal >= 0){
      flagUpdate()
      return
    }
    if (stepVal < 0){
      setNewTransition(true);
    }
  }


  function fastForward(){
    if (tmRunning){
      const stepVal = tm.step()
      if (stepVal >= 0 && tmRunning){
        flagUpdate()
        return
      }
      if (stepVal < 0){
        setNewTransition(true);
        setTmRunning(false);
      }
    }
  }

  function startFastForward(){
    setTmRunning(!tmRunning);
  }

  function handleResetEverything(){

    setTm(new TuringMachine())
    setNewTransition(false)
  }

  function handleResetStateAndInput(){
    tm.currentPosition = 0
    tm.currentState = tm.initialState
    tm.tape.tape[0] = []
    tm.tape.tape[1] = []
    tm.configHistory = []
    flagUpdate()
  }

  


  /*-----------------------------------------------------------------------------------------
        RENDER FUNCTIONS 
  -------------------------------------------------------------------------------------------*/

   

  function PlayPause(){

    const ShowPlayButton = ()=>{
      if (tmRunning){ 
        return(
            <button 
              className="tmControl tmControlFastForward" 
              onClick={
                ()=>{
                  setTmRunning(false)
                }
              }>
                <img className="mediaButton" src={pauseImg} alt="pause the tm">
                </img>
              </button>
        )
      }
      return (
        <div>
          <button 
            className="tmControl tmControlPlay" 
            onClick={handleNextStep}>
              <img className="mediaButton" src={playImg} alt="do one step">
              </img>
          </button>
          <button
            className="tmControl"
            onClick={startFastForward}
            alt="set the tm to running"
          >
            <img className="mediaButton" src={ffwImg} alt="fast forward icon">
            </img>
          </button>
        </div>
      )
    }

    if (tm.currentState !== tm.finalState){
      return <ShowPlayButton></ShowPlayButton>
    }else{

      return(
        <div style={{fontSize: "30px"}}>
          TM halted. 
          <a href="/#" 
            onClick={()=>{handleResetStateAndInput()}}
            className="button warn">
              reset?
          </a>
        </div>
      )
    }

  }


  const ConditionalNewTransitionForm = ()=>{
    if (!newTransition) return null
    return <NewTransitionForm tm={tm} setNewTransition={setNewTransition} handleConfirm={startFastForward}/>
  }

  const ConditionalPlayBackButtons = () =>{
    if (newTransition) return null
    return (
    <div className="tmControls">
      <div className="playbackControls">
        <PlayPause></PlayPause>
      </div>
    </div>
    )
  }
  return (
    <div>
      <div key="1" className="tmConfigDisplayWrap">
      <h1>REACTive Turing Machine</h1>  
      </div>
      <div key="2" className="tmConfigDisplayWrap">

      <div className="configDisplayWrap">
        <div className="justCellWrap">
      <TmConfigDisplay
        flagUpdate = {flagUpdate}
        displayElemCount = {displayElemCount}
        handleConfirm={startFastForward}
        tm={tm}
      ></TmConfigDisplay>
      </div>  
      <div style={{textAlign: "right"}}>
          <ExportImportGroup 
              tm={tm}
              setTm={setTm}
              flagUpdate={flagUpdate}
              handleResetStateAndInput={handleResetStateAndInput}
              handleResetEverything={handleResetEverything}
              />
          </div>     
      <ConditionalNewTransitionForm></ConditionalNewTransitionForm>
      <ConditionalPlayBackButtons></ConditionalPlayBackButtons>
      <TmCurrentConfig tm={tm} flagUpdate={flagUpdate}>

      </TmCurrentConfig>
      </div>
    </div>
  </div>
  );
}

export default App;
