import React, { useState } from "react"

export default function(props){

    function changeInitialState(name){
        if (tm.currentState === props.tm.initialState){
            props.tm.currentState = name
        }
        props.tm.initialState = name
       
        props.flagUpdate()
    }

    function changeFinalState(name){
        if (tm.currentState === props.tm.finalState){
            props.tm.currentState = name
        }
        props.tm.finalState = name
        props.flagUpdate()
    }

    function changeCurrentState(name){
        props.tm.currentState = name
        props.flagUpdate()
    }

    function handleDeleteTransition(state, symbol){
        delete tm.transitionFunction[state][symbol];
        if (Object.keys(tm.transitionFunction).length === 0) delete tm.transitionFunction[state]
        props.flagUpdate()
    }
 
 
    function RenameState(props){
        const [name,setName] = useState(props.name);
        const [editing, setEditing] = useState(false);

        if (!editing){
            return <div>
                {name}
                {' '}<a href="/#" style={{color:"white"}} onClick={()=>setEditing(true)}>change</a>
            </div>
        }else{
            return <div>
            <input value={name} onChange={(e)=>setName(e.target.value)}></input>
            {' '}
            <a href="/#" 
                style={{color:"white"}} 
                onClick={
                    ()=>
                        {props.stateNameToChange(name)
                        setEditing(false)
                    }
                }
            >
                ok
            </a>
        </div>
        }
        
    }



    const tm = props.tm
    const states = []
    const transitions = []


    if (tm.transitionFunction){
        for (let stateKey of Object.keys(tm.transitionFunction)){
            states.push(stateKey)
            for(let tapeSymbol of Object.keys(tm.transitionFunction[stateKey])){
                const writeSymbol = tm.transitionFunction[stateKey][tapeSymbol][1]

                const targetState = tm.transitionFunction[stateKey][tapeSymbol][0]
                if(!states.includes(targetState)){
                    states.push(targetState);
                }
                const headerMovement = tm.transitionFunction[stateKey][tapeSymbol][2]
                const renderSymbol = tapeSymbol?(tapeSymbol===""?"B":tapeSymbol):"B";
            transitions.push(
                <div key={stateKey+tapeSymbol}>
                    {`(${stateKey},${renderSymbol})\u2192 (${targetState},${writeSymbol},${headerMovement})`}
                    {' '}
                    <a 
                        href="/#" 
                        style={{color:"white"}} 
                        onClick={
                            ()=>handleDeleteTransition(stateKey,tapeSymbol)
                        }
                    >
                        delete
                    </a>
                </div>)
            }
        }
    }

    states.push(tm.finalState)
    
    
    return(
    <div>
        <h2>Current Configuration:</h2>
        <div>
            <h4 style={{display: "inline"}}> 
                Current state:
            </h4>
            <RenameState 
                stateNameToChange={changeCurrentState} 
                name={tm.currentState}
            >
            </RenameState>
        </div>
        <div>
            <h4>
                 Initial state:
            </h4>
            <RenameState 
                stateNameToChange={changeInitialState} 
                name={tm.initialState}
            >
            </RenameState>
        </div>
        <div>
            <h4> 
                Final state:
            </h4>
            <RenameState 
                stateNameToChange={changeFinalState} 
                name={tm.finalState}
            >
            </RenameState>
        </div>
        <div>
            <h4>States:</h4> 
            {'{'} {states.join(', ')}{' }'}
        </div>
        <div>
            <h4>Transitions:</h4>
            {transitions}
        </div>
        <div>
            <h4>
                TM-Configuration-Sequence:
            </h4>
            {tm.configHistory.map((v,i)=><div key={i}><b>{`${i}: `}</b>{`${v}`}</div>)}
        </div>
    
    </div>)
}